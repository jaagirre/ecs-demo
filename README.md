# ML Cloud deploy

git clone https://gitlab.com/macc_ci_cd/iraunkor/ml_deploy_cloud.git


## Prueba local
```bash
docker build -t yolov5-fastapi-froga .
```

```bash
docker run -p 8080:8000 yolov5-fastapi-froga
```

Navegar y comprobar las URLs de fastAPI

## Despliegue maual  en EC2

- Crear maquina EC2
- Conexion SSH 
- Clonar repositorio
- Arrancar imagen
- Comprobar acceso publico
- Escalabilidad: Crear un Target group  y ELB 

## Despliegue manual ECS

![lab-ci1](doc/images/lab_ci1.png)

En este paso vamos a realizar un despliegue manual. 

Para el despliegue en este laboratorio utilizaremos Amazon AWS Academy y los servicios RDS y ECS. 
* RDS (Relational Database Service) es un servicio de base de datos relacionales.
* ECS (Elastic Container Service) es un servicio de administración de contenedeores compatible con los contenedores Docker. 

Antes de hacer el despligue probaremos el repositorio en un máquina EC2 (Amazon Elastic Compute Cloud)

## Prueba inicial lanzando sin docker-compose en AWS EC2

Ahora ejecutamos este docker file en la máquina bastión EC2 (Amazon Elastic Compute Cloud) o en vuestra propia máquina definiendo el Endpoint de la base de datos.
Si el RDS es público podreis acceder desde vuestra máquina, en caso contrario tendreis que ir desde una EC2 con acceso a la red privada del RDS. En mi caso lo haré desde el bastión EC2.

Estos son los pasos:
* Clonar el repositorio (con el nuevo dockerfile de producción)
* Probar conectividad a base de datos
* Crear y ejecutar contenedor
* Probar la aplicación (curl...)


```bash

ssh -i ~/Deskargak/learnerlab.pem ec2-user@44.212.227.52
       __|  __|_  )
       _|  (     /   Amazon Linux 2 AMI
      ___|\___|___|

sudo amazon-linux-extras install docker
sudo service docker start
sudo usermod -a -G docker ec2-user

git clone https://gitlab.com/macc_ci_cd/ci_cd/lab_ci1.git
cd lab_ci1
docker run --name fastapi-tdd -e APP_ENVIRONMENT=production  -e DATABASE_PROD_URL=$DATABASE_URL -p 5003:8000  -d jaagirre/fastapi-tdd:latest
curl http://127.0.0.1:5003/health-check/
{"message":"OK","environment":"production","debug":false,"test":false}[
 curl http://44.212.227.52:5003/health-check/
{"message":"OK","environment":"production","debug":false,"test":false}
```

Hasta ahora hemos desplegado de forma manual como se realizó en el primer semestre.

## Desplegando en AWS ECS

Ahora que hemos probado que el sistema funciona, vamos a subir la imagen al servicio AWS ECR.
Primero creamos un repositorio, por ejemplo denominado fatsapi-tdd.  El siguiente paso es subir/push de la imagen del docker al ECR. Para esto primeros tenemos que logearnos en el Registro ECR, luego tageamos correctamente la imagen y finalmente hacemos un
push. Recordar que para poder ejecutar los comando cli aws desde cualquier ordenador tenemos que tener las credenciales configuradas en ~/.aws/credentials 
o en caso utilizar Bastion para las operaciones asignar un rol adecuado a la máquina EC2.

### Crear el repositorio o registro de imagenes en AWS ECR
Como vemos en la imagen, creamos el repositorio o registro de imagenes Docker mediante el servicio AWS ECR. Y utilizaremos este registro para gestionar las imagenes de nuetro servicio de Posts. A la imagen le llamaremos, por ejemplo, fastapi-tdd.

![ECR](doc/images/ecr1.png)
![ECR](doc/images/ecr2.png)

### Subir la imagen docker de producción al registro de ECR

Una vez creado el registro copiamos el identificador y el siguiente paso será subir al registro la imagen de producción. Para esto debemos de realizar lo siguiente: 1) nos logeamos, 2) creamos la imagen local de producción 3) tageamos con el nombre del registro ECR y finalmente 4)hacemos push de la imagen.

```bash
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 662960992834.dkr.ecr.us-east-1.amazonaws.com
    1 | docker login --username AWS --password-stdin 662960992834.dkr.ecr.us-east-1.amazonaws.com
    WARNING! Your password will be stored unencrypted in /home/joseba/.docker/config.json.
    Configure a credential helper to remove this warning. See
    https://docs.docker.com/engine/reference/commandline/login/#credentials-store
    
    Login Succeeded

docker build -f project/Dckerfile.prod -t jagirre/fastapi-tdd ./project
docker tag jaagirre/fastapi-tdd:latest 662960992834.dkr.ecr.us-east-1.amazonaws.com/fastapi-tdd:latest
docker image credentials
  jaagirre/fastapi-tdd                                          latest               691e4709d4db   35 seconds ago      350MB
  662960992834.dkr.ecr.us-east-1.amazonaws.com/fastapi-tdd      latest               691e4709d4db   35 seconds ago      350MB
docker push 662960992834.dkr.ecr.us-east-1.amazonaws.com/fastapi-tdd:latest
    4d5f738025a7: Pushed 
    4c2b77c2cff5: Pushed 
    latest: digest: sha256:e9120c3b60b28b1e079fc32262856f4b782f43e729362ced384b2b99153ae6df size: 3045

``` 

![ECR](doc/images/ecr3.png)

### Desplegar en ECS FARGATE

Una vez creado el ECR y que disponemos de la imagen en el registro, el siguiente paso será desplegar el contenedor mediante el servicio ECS de AWS. Para esto crearemos una definición de tarea, la cual especifica cómo queremos ejecutar el contenedor que se va a desplegar. Una vez creada la definición de la tarea, se crean los recursos para desplegar esa tarea: cluster, servicio y tarea.

Primero empezamos con la definición de una tarea (parecido a un Pod de K8s), importante indicar la imagen que utilizaremos **662960992834.dkr.ecr.us-east-1.amazonaws.com/fastapi-tdd:latest** junto con el puerto que expondremos, en este caso el 8000. Además de esto tambien tendremos que indicar como ejecutará el contenedor, en este caso especificando las variables de entorno APP_ENVIRONMENT Y DATABASE_PROD_URL en el caso de producción. También 
seleccionaremos el tipo de entorno de ejecución ECS FARGATE y los roles a asociar al contendor/tarea. En este caso el rol que nos da ACADEMY **LabRole**.  

![ECS](doc/images/ecs2.png)
![ECS](doc/images/ecs3.png)

Una vez tenemos creadas las tareas, el siguiente paso es crear el cluster ECS donde ejecutaremos la tarea;-)
Primero creamos el cluster, en este caso es importante crear el cluster en la misma VPC que el RDS de postgres, 
ya que no le hemos dado acceso público, y solo se puede acceder a el desde la misma VPC con ip privada;-)

![ECS](doc/images/ecs1.png)

Después el siguiente paso es crear el servicio dentro del cluster. Un servicio ECS pone en marcha una tarea, 
y es muy parecido a un servicio k8s. A la hora de crear el servicio especificar el Launch Type como Fargate.
Y en la configuración de despliegue elegir el tipo **Service**.

![ECS](doc/images/ecs4.png)

Después selecionamos la tarea que queremos desplegar. En este caso, la que hayamos creado previamante 
con la imagen que tenemos en el ECR. También podemos indicar cuantas réplicas queremos, en este caso eligiremos 1.
También seleccionamos la VPC donde queremos que se lance. Y también podemos asignarle una IP pública al servicio. En nuestro caso como accederemos vía un ELB (Elastic Load Balancing) de AWS (para poder tener autoescalabilidad y alta disponibilidad 
si nos hace falta), no hace falta crear un punto/IP público. Después seleccionamos las opciones de LoadBalancer. 
El servicio se puede lanzar sin ELB, pero para facilitar disponibilidad y réplicas en un futuro, mejor crear con ELB.
Al configurar el ELB recordar que expondremos el puerto 8080 (cuidado en la imagen pone 8000, usar el 8080 debido a las restricciones del firewall) y que la URL de health-check de nuestro fastapi es 
**/health-check/**. También le daremos el nombre del target group. No olvideis actualizar el security group
asociado al ELB para permitir el acceso al puerto 8080 (cuidado en la imagen pone 8000, usar el 8080 debido a las restricciones del firewall). 
Nota: si se quiere mantener el puerto 8000, habría que acceder desde el bastión.


![ECS](doc/images/ecs10.png)
![ECS](doc/images/ecs11.png)
![ECS](doc/images/ecs12.png)

Ahora creamos el servicio en el cluster además de crear el ELB, Target group y meterá los 
contenedores/tareas en el target group;-)

![ECS](doc/images/ecs13.png)
![ECS](doc/images/ecs14.png)
![ECS](doc/images/ecs15.png)


### Probar cómo ha ido el despliegue

Podemos ver como ha ido el despligue, esto es importante para cuando en el futuro actualizemos las imagenes.
![ECS](doc/images/ecs16.png)

En el servicio podemos ver su tarea y los logs que han creado sus contenedores, incluso la ip del contenedor.
![ECS](doc/images/ecs17.png)

Despues desde el servicio podemos ir al ELB y ahí ya mediante la IP pública o el nombdre DNS ya tendremos acceso al 
servidor del contenedor;-)
![ECS](doc/images/ecs18.png)

Si navegamos al target group en EC2 veremos como tenemos una instancia de docker dentro de el, con su ip privada.
![ECS](doc/images/ecs19.png)

En la siguiente imagen accedemos al servidor vía el ELB que direcciona al servicio ECS Fargate;-)
![ECS](doc/images/ecs20.png)
![ECS](doc/images/ecs21.png)

Tener cuidado con el security groups del vpc por defecto;-)

Podreis probar a hacer unos POST y luego hacer un get para ver que el acceso al RDS de postgres es correcto.

![ECS](doc/images/ecs24.png)


En el siguiente lab veremos como actualizar con nuevas versiones nuestro servicio ECS, 
además de automatizar via TERRAFORM la creación de la infrastructura.

# LAB02 - TDD based CI with Dockerized FastAPI Application

Vamos a partir del resultado final de laboratorio 1 y comenzar con la integración continua.
En este laboratorio vamos a crear el pipeline de CI (integración continua) en GitLab para nuestra aplicación ;-)

Antes de nada crear un repositorio vacio de gitlab. Despues crear una carpeta local vacia , copiar las carpetas *doc/*, *model/*, *yolov5* y los ficheros , excepto el fichero *.gitlab-ci.yml*. Este ultimo iremos creandolo en los siguiente pasos;-).

Pipeline de 3 _stages_ y depliegue manual en cloud. 
![3-stages workflow with manual deploy](doc/images/pipeline3stages.png)

Pipeline de 4 _stages_ que realiza el depliegue en cloud desde el pipeline.
![4-stages Workflow](doc/images/pipeline4stages.png)



## Pipeline para Integración continua (CI)

Nuestro siguiente objetivo es crear un pipeline inicial de CI (Integración continua) en gitlab que genere 
el contenedor de la aplicación, lo valide y haga el delivery final tageando correctamente el contenedor.

Ahora vamos a comenzar con el pipeline de CI. En este paso vamos a automatizar el proceso de  validación del software y su posterior creación mediante el uso de un PIPELINE. Para automatizar el proceso se utiliza GITLAB. El objetivo del PIPELINE (que tiene 3 pasos o stages) es:

- Crear la imagen del Contenedor --> [Stage build](#stage-build)
- Ejecutar las validaciones --> [Stage test](#stage-test)
- En caso satisfactorio subir las imágenes al registro de Contenedores --> [Stage delivery](#stage-delivery)


## Stage build 

Empezamos creando el fichero pipeline de gitlab .gitlab-ci.yml. Y definimos el stage de build y dentro de él una tarea
para crear el contenedor de producción. De momento creamos una tarea que simplemnte se logea en 
el  registro de contenedores del proyecto, hace un pull para obtener la última IMAGEN y utilizar su cache en la creación del nuevo contenedor (utilizar el cache permite reducir tiempos). Y finalmente hace push para guardar la imagen. 


```yml
image: docker:stable

stages:
  - build

variables:
  IMAGE: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}

build:
  stage: build
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $IMAGE:latest || true
    - cd project/
    - docker build
        --cache-from $IMAGE:latest
        --tag $IMAGE:latest
        --file Dockerfile.prod
        .
    - docker push $IMAGE:latest
```

Y ahora al hacer git push vemos como se ejecuta el pipeline en gitlab. Y ha creado el contenedor, tageago como :latest.

![CI](doc/images/pipeline1.png)

Y podemos ver el output creado por la ejecución del script de la tarea/job ejecutada y que ha generado en este caso el contenedor.
![CI](doc/images/pipelines4.png)
![CI](doc/images/pipelines5.png)

Cada proyecto tiene un registro de contenedores. Aquí podemos ver donde ha alojado el conetendor creado mediante el pipeline.

![CI](doc/images/pipeline2.png)
![CI](doc/images/pipeline3.png)
![CI](doc/images/pipelines4.png)


## Stage test 

Una vez que ya hemos visto comos hemos creado un contenedor, ahora vamos a validar que su código es correcto.
Para esto crearemos un nuevo job y stage que denominaremos de momento test. Agregaremos el siguiente código al .gitlab-ci.yml. Lo importante ahora es que no queremos trabajar con el docker-compose, la idea es utilizar el contenedor indpendientemente, y utilizaremos el service de gitlab para utilizar en los test una imagen docker de postgres. Aquí utilizando la imagen creada anteriormente como base de la tarea/job ejecutamos pytest, flake8, black e isort.

```yml
stages:
  - build
  - test # esto es lo nuevo

test:
  stage: test
  image: $IMAGE:latest
  services:
    - postgres:latest
  variables:
    APP_ENVIRONMENT: local_test
    POSTGRES_DB: web_test
    POSTGRES_USER: runner
    POSTGRES_PASSWORD: ""
    POSTGRES_HOST_AUTH_METHOD: trust
    DATABASE_LOCAL_TEST_URL: postgresql+asyncpg://runner@postgres:5432/web_test
  script:
    - cd /home/app/web
    - pytest tests -p no:warnings
    - flake8
    - black . --check
    - isort ./**/*.py --check-only
```
Y ahora al hacer push vemos como se ejecutan dos tareas/jobs en serie. Y podemos ver los logs;-)

![CI](doc/images/pipelines5.png)
![CI](doc/images/popeline7.png)
![CI](doc/images/popeline8.png)
![CI](doc/images/popeline6.png)

Para que los build se ejecuten solo cuando cambia algo del proyecto hemos incluido unos filtros en las tareas.

```yml
  only:
    refs:
      - master
    changes:
      - project/**/*
```

Ahora vemos que al cambiar el -.gitlab-ci.yml solo ejecuta el job test sin hacer build.
![CI](doc/images/pipelines9.png)


## Stage delivery 

Una vez que ya tenemos el contenedor construido y validado, y tenemos la base para crear pipelines de GITLAB
pasaremos a crear un stage de Delivery. El objetivo de este fase es reetiquetar el contenedor como lastest. 
Tambien etiquetaremos los contendores con un hash del commit y si existen tags con un tag.


Lo primero que hacemos es crear unas variables 
en base a los commits que nos permiten etiquetar los nombres 

```yml
stages:
  - build
  - test
  - delivery

variables:
  IMAGE_LATEST: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:latest
  IMAGE_RELEASE: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:release_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}
  IMAGE_TEST: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:test_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}

```

 - IMAGE_TEST => Nombre de la imagen que vamos a validar
 - IMAGE_RELEASE =>  Nombre de la imagen validada y a desplegar en producción
 - IMAGE_LATEST => Nombre de la ultima imagen en producción

Y ahora modificamos los diferentes stages con los nuevos nombres y creamos el nuevo stage delivery. El stage delivery simplemente reetiquetará el nombre del contenedor.


```yml
build:
  stage: build
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $IMAGE_LATEST || true
    - cd project/
    - docker build
        --cache-from $IMAGE:latest
        --tag $IMAGE_TEST
        --file Dockerfile.prod
        .
    - docker push $IMAGE_TEST
  only:
    refs:
      - master
    changes:
      - project/**/*
      - .gitlab-ci.yml

delivery:
  stage: delivery
  image: docker:stable
  services:
    - docker:dind
  variables:
      DOCKER_DRIVER: overlay2
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $IMAGE_TEST
    - docker tag $IMAGE_TEST $IMAGE_RELEASE
    - docker push $IMAGE_RELEASE
    - docker tag $IMAGE_RELEASE $IMAGE_LATEST
    - docker push $IMAGE_LATEST

  only:
    refs:
      - master
    changes:
      - project/**/*
      - .gitlab-ci.yml


```
Y aquí tenemos el resultado:

![CI](doc/images/ci_11.png)
![CI](doc/images/ci12.png)

Ahora también indicamos que sólo se ejecute con commits tageados.


```yml
image: docker:stable

variables:
  IMAGE_LATEST: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:latest
  IMAGE_RELEASE: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:release_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}
  IMAGE_TEST: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:test_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}

stages:
  - build
  - test
  - delivery


build:
  stage: build
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $IMAGE_LATEST || true
    - cd project/
    - docker build
        --cache-from $IMAGE:latest
        --tag $IMAGE_TEST
        --file Dockerfile.prod
        .
    - docker push $IMAGE_TEST
  only:
    refs:
      - tags
    changes:
      - project/**/*
      - .gitlab-ci.yml
  except:
    - branches

test:
  stage: test
  image: $IMAGE_TEST
  services:
    - postgres:latest
  variables:
    APP_ENVIRONMENT: local_test
    POSTGRES_DB: web_test
    POSTGRES_USER: runner
    POSTGRES_PASSWORD: ""
    POSTGRES_HOST_AUTH_METHOD: trust
    DATABASE_LOCAL_TEST_URL: postgresql+asyncpg://runner@postgres:5432/web_test
  script:
    - cd /home/app/web
    - pytest tests -p no:warnings
    - flake8
    - black . --check
    - isort ./**/*.py --check-only
  only:
    refs:
      - tags
    changes:
      - project/**/*
      - .gitlab-ci.yml
  except:
    - branches

delivery:
  stage: delivery
  image: docker:stable
  services:
    - docker:dind
  variables:
      DOCKER_DRIVER: overlay2
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $IMAGE_TEST
    - docker tag $IMAGE_TEST $IMAGE_RELEASE
    - docker push $IMAGE_RELEASE
    - docker tag $IMAGE_RELEASE $IMAGE_LATEST
    - docker push $IMAGE_LATEST

  only:
    refs:
      - tags
    changes:
      - project/**/*
      - .gitlab-ci.yml
  except:
    - branches
```

```bash
git tag -a v0.0 -m 'Primer tag'
git push origin master --tags
```

# Paso 11 (OPCIONAL): Despligue continuo (CD) 

En esta sección realizaremos el despliegue en ECS del contendor tageado en delivery. Primero lo haremos de
forma manual, y después lo automatizaremos vía un sencillo script. 

TENENR EN CUENTA QUE DONDE PONE **/macc_ci_cd/ci_cd/labtdd-fastapi-example** TENDRIS QUE CAMBIARLO POR EL NMBRE DE VUETSRI REPOSITORIO EN MI CASO **https://gitlab.com/macc_ci_cd/iraunkor/ml_deploy_cloud**.

Estos son los pasos:
- [Despliegue de forma manual de nueva versión](#despliegue-de-forma-manual-de-nueva-version)
- [Realizar el despliegue utilizando un script](#realizar-el-despliegue-utilizando-un-script)
- [Despliegue en el pipeline](#despliegue-en-el-pipeline)


## Despliegue de forma manual de nueva versión

En el laboratorio 1 en el paso 9 realizasteis un despliegue manual. Ahora la idea es redesplegar, es decir, desplegar una nueva versión, para ello hay que crear una nueva revisión en ECS. 

Antes de automatizar el despliegue en ECS
es conveniente entender como se despliega manualmente una nueva versión. 
Los pasos son lo siguientes:

1. Disponer de una versión validada en delivery en gitlab
2. Hacer un push a ECR con la nueva imagen
3. Crear una nueva revisión de la tarea ECS
4. Y en el cluster y servicio realizar un nuevo despliegue
5. El despliegue se realiza vía rollout. Es decir crea una nueva instacia, durante un corto periodo conviviran las dos versiones y si el healthcehck ha funcionado, terminará la instancia anterior dejando únicamente la nueva versión.

Como ya disponemos de la versión validada y tageada como release y latest. Lo primero que hacemos es descargar la versión.

```bash
docker login registry.gitlab.com
docker pull   registry.gitlab.com/macc_ci_cd/ci_cd/labtdd-fastapi-example:latest
```
Y retageamos (renombramos) el contenedor con el nombre del registro ECR que hemos creado anteriormente.
En este ejemplo hemos tageago como froga, pero lo lógico sería utilizar el mismo TAG que se ha utilizado en el pipeline.

```bash
export REPOSITORY_URL=662960992834.dkr.ecr.us-east-1.amazonaws.com/fastapi-tdd
docker tag  registry.gitlab.com/macc_ci_cd/ci_cd/labtdd-fastapi-example:latest $REPOSITORY_URL:froga
```
Y una vez que tenemos todo preparado el siguiente paso es logearnos en ECR de AWS. Para ello utilizamnos la siguiente 
fucnión de AWS CLI. Recordar que para ejecutar este comando tenemos que tener las credenciales de AWS en el sistema.
Lo habitual suele ser configurarlo en **~/.aws/credentials**, pero ya que en el pipleine de CI lo haremos vía variables de entorno, ahora también lo haremos.
```bash
export AWS_ACCESS_KEY_ID=ASIAZGGESUT
export AWS_SECRET_ACCESS_KEY=dOvnI8TkdgyK+5zlhl
export AWS_SESSION_TOKEN=FwoGZXIvYXdzEgmGmCRms7/E=
$(aws ecr get-login --no-include-email --region us-east-1)
    WARNING! Using --password via the CLI is insecure. Use --password-stdin.
    WARNING! Your password will be stored unencrypted in /home/joseba/.docker/config.json.
    Configure a credential helper to remove this warning. See
    https://docs.docker.com/engine/reference/commandline/login/#credentials-store
    Login Succeeded
```
Y ahora ya podemos subir la nueva imagen. 
```bash
docker push  $REPOSITORY_URL:froga
    4c2b77c2cff5: Layer already exists 
    froga: digest: sha256:2815335345e4a42e66d4adbb9ec565855abfc57bd23d96ceabf04540d557c2f5 size: 3045
```
Y lo comprobamos en la consola de AWS ECR.

![ECR](doc/images/ci_stage_deploy_ecr.png)

Ahora vamos a redesplegar manualmente esta versión. Para ello primero vamos a ECS y selecionamos el Task definition que 
utilizamos en el despliegue anterior. Y ahora decimos que nos cree una nueva revisión en base al anterior, y al crearlo
le indicaremos el nuevo contenedor, en mi caso tageado como **:froga**. En este caso selecionaremos crear una nueva revision con json, ya que luego para automatizar vía script utilizaremos el formato json;-)
![ECR](doc/images/ci_revision_ecs.png)
![ECR](doc/images/ci_revision_ecs2.png)

Y en este caso tenemos la definción de la tarea donde seleccionamos el nuevo contenedor.
![ECR](doc/images/ci_revision_ecs3.png)

Y ahora una vez creada la nueva revisión, con el numero correspondiente **froga:9**, vamos al servicio de nuestro cluster, y  vamos a la tarea que tenemos desplegadas. Y seleccionamos crear tarea.
![ECR](doc/images/ci_revision_ecs4.png)

En tarea simplemente seleccionamos la última revision de definición de tarea creada previamente, **froga:9**.
Por defecto, ya viene selecionada;-) , y le damos a  crear. E inmediatamente vemos como comienza el  rollout.
![ECR](doc/images/ci_revision_ecs5.png)
![ECR](doc/images/ci_revision_ecs6.png)
![ECR](doc/images/ci_revision_ecs8.png)

Y una vez toda va bien elimina la revisión anterior. Si navegamos al endoint del ELB veremos como han ido 
desplegandose, la nueva versión. Para comprobar esto cambia lo que devuelve el endpoint del health-check y así vereis como el ELB en el rollout va cambiando una versión por la otra;-)

![ECR](doc/images/ecs5.png)


## Realizar el despliegue utilizando un script 

Ahora que hemos visto el proceso para realizar el despliegue de una nueva version vía web, pasamos a crear un script bash que hace lo mismo. 

Es convieniente realizar este script para linux, para luego poder reutilizarlo en el pipeline en el siguiente paso. En caso de que useis Windows, para probar en local en este paso utilizar un entorno virtual, Windows subsystem for linux o un docker.  

Para realizar el despliegue vía script, utilizaremos un JSON donde definiremos la nueva tarea a desplegar.


Los primeros pasos son equivalentes. Lo primero será configurar variables de entorno que después utilizaremos y 
actualizaremos automáticamente en el pipleine de CI de gitlab. Llamaremos al script **update_ecs.sh**.
Antes de ejecutar el .sh supondremos que ya hemos hecho pull de la imagen 
**docker pull   registry.gitlab.com/macc_ci_cd/ci_cd/labtdd-fastapi-example:latest**.
Por un lado definimos las credenciales de AWS

```bash
export AWS_ACCESS_KEY_ID=ASIAZGGESUT
export AWS_SECRET_ACCESS_KEY=dOvnI8TkdgyK+5zlhl
export AWS_SESSION_TOKEN=FwoGZXIvYXdzEgmGmCRm2xPWRz8xDqFLDwaRXG0m0G0nh/QysB7SmRbYhK/1wqc7kUlNzddAmJbIRNAVo/OlpulL73dWbrIEJQDauxfE3llbTqiOMIrJGUm1BR6muRjCS4vJWA7TWrMAGUPgI54JBF3zQdqMyz8WKSHxKes4b7iLfIqi4EPb/wlKSonAxOzs98eDdjNImgmupMDrtVzNhstnkrJr+yj7cwIb++50CvbNgNvyWN4Vv9zvBekgy/Wzl8N7645c097KNy33p4GMi19tLJaSBwDJWRJZ3qdohTuquMvLU1Uwnmue88rh/fi0Jajfk6y+if/eJMs7/E=
```

Despues definimos la informacoión de ECS, junto con el nombre del repositorio ECR y el nombre de la última imagen creada en gitlab.

```bash

export CI_AWS_ECS_CLUSTER=froga
export CI_AWS_ECS_SERVICE=froga
export CI_AWS_ECS_TASK_DEFINITION=froga

export REPOSITORY_URL=662960992834.dkr.ecr.us-east-1.amazonaws.com/fastapi-tdd
export IMAGE_TAG=registry.gitlab.com/macc_ci_cd/ci_cd/labtdd-fastapi-example:latest
```

Una vez tenemos configurado las variables de entorno y hemos realizado el pull de la imagen de delivery de gitlab, lo siguiente es agregar al script el logging en ECR de AWS, tagear la imagen con el nombre del ECR y hacer el pull.

```bash

docker tag  registry.gitlab.com/macc_ci_cd/ci_cd/labtdd-fastapi-example:latest $REPOSITORY_URL:froga
$(aws ecr get-login --no-include-email --region us-east-1)
docker push  $REPOSITORY_URL:froga
```
Ahora vamos con las operaciones nuevas. Lo primero que hacemos es pedir a ASW ECS el json con la definción de la 
tarea de la cual queremos hacer una nueva versión/revisión. Y lo que hacemos es crear un fichero con la definición, **input.json**.

```bash
echo `aws ecs describe-task-definition --task-definition  $CI_AWS_ECS_TASK_DEFINITION --region us-east-1` > input.json
```

Una vez tenemos el input.json, utilizaremos la utilidad **jq** (json query: instalar con apt get) para buscar el campo que define la imagen y modificamos su valor con la imagen que queramos, y sobreescribimos el **input.json**. el json con el nombre de la nueva imagen;-)
```bash
echo $(cat input.json | jq '.taskDefinition.containerDefinitions[].image="'$REPOSITORY_URL':'froga'"') >  input.json
```
Y quitamos ciertos valores que se generan al crear la definición
```bash
echo $(cat input.json | jq '.taskDefinition') > input.json
echo $(cat input.json | jq  'del(.taskDefinitionArn)' | jq 'del(.revision)' | jq 'del(.status)' | jq 'del(.requiresAttributes)' | jq 'del(.compatibilities)') > input.json
```
Y utilizando el json creamos la nueva definición, y obtenemos el número de revisión
```bash
aws ecs register-task-definition --cli-input-json file://input.json --region us-east-1
revision=$(aws ecs describe-task-definition --task-definition $CI_AWS_ECS_TASK_DEFINITION --region us-east-1 | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//' | cut -d "," -f 1)
```

Y finalmente utilizando el nombre de la tarea y el número de la revisión, actualizamos la tarea del servicio;-)
```bash
aws ecs update-service --cluster $CI_AWS_ECS_CLUSTER --service $CI_AWS_ECS_SERVICE  --task-definition $CI_AWS_ECS_TASK_DEFINITION:$revision --region us-east-1
echo 'Done'
```

Ahora vamos a probar el script. Para ello vamos a realizar una iteración completa TDD. El objetivo será sencillo, 
simplemente modificar la respuesta del healthcheck agregando al mensaje de respuesta **v2**.
Primero modificamos el código tanto del test healtcheck como de la ruta. Le añadiremos al mensaje de 
respuesta **v2**.

```bash
class TestRoutes:
    def test_health_check(self, client):
        response = client.get("/health-check/")
       
        assert response.status_code == 200
        assert response.json() == {
            "message": "OKv2",
            "debug": True,
            "environment": "local",
            "test": False
            # "database_url": "postgresql+asyncpg://postgres:postgres@web-db:5432/web_dev",
        }


@router.get("/health-check/")
async def healthcheck(
    config: BaseConfig = Depends(load_config), db: AsyncSession = Depends(get_session)
):
    return {
        "message": "OKv2",
        "environment": config.APP_ENVIRONMENT,
        "debug": config.DEBUG,
        "test": config.TESTING
        # "database_url": config.SQLALCHEMY_DATABASE_URL,
    }
```
Y ahora probamos en local (ejecutando el local_pipeline), antes de hacer commit que toda va bien, y que no hemos roto nada;-)
```bash
docker-compose up -d --build
docker-compose exec web pytest tests -p no:warnings --cov
  -------------------------------------------------------------------
  TOTAL                                             414     64    85%
  ================================================ 22 passed in 6.11s =============================================
docker-compose exec web  flake8 -v 
  flake8.main.application   MainProcess    439 INFO     Found a total of 2 violations and reported 0
docker-compose exec web black . --check 
    All done! ✨ 🍰 ✨
    22 files would be left unchanged.
docker-compose exec web isort   --check-only .  -v
  SUCCESS: /usr/src/app/tests/conftest.py Everything Looks Good!
```
Ahora que vemos que el cambio no ha afectado, realizamo el commit y push que lanzara el pipeline de CI hasta Delivery.
Recordar que para que ejecute el pipeline, ahora tenemos que tagear;-)
```bash
git tag -a v0.10.2 -m 'Healthcheck  message with: Okv2'
git push origin master --tags
```
![CD](doc/images/tag_v0.10.2.png)
![CD](doc/images/tag_v0.10.2-passed.png)
![CD](doc/images/tag_v0.10.2-containers.png)

Ya tenemos el nuevo contenedor. Ahora ejecutamos el script para actualizar el ecs.
Para ello primero establecemos las credenciales y valores adecuados en el script.
Y antes de lanzar el script 
descargamos la nueva imagen y para esta prueba la tagearemos como **$REPOSITORY_URL:froga1**. Por lo tanto modificar el tag en el script.

```bash
docker login registry.gitlab.com
docker pull   registry.gitlab.com/macc_ci_cd/ci_cd/labtdd-fastapi-example:latest
```
El siguiente es el trozo de script que modificamos con el nuevo tag
```bash
docker tag  registry.gitlab.com/macc_ci_cd/ci_cd/labtdd-fastapi-example:latest $REPOSITORY_URL:froga1
$(aws ecr get-login --no-include-email --region us-east-1)
docker push  $REPOSITORY_URL:froga1
...
echo $(cat input.json | jq '.taskDefinition.containerDefinitions[].image="'$REPOSITORY_URL':'froga1'"') >  input.json
```

Y lanzamos el script
```bash
sh update_ecs.sh
```
Y vemos como ha estado haciendo el despliegue
![CD](doc/images/tag_v0.10.2-deploy.png)

Ahora ya solo nos queda ir al endpoitn del ELB de nuestro ECS de AWS y comprobar que ahora el healthceck nos devuelve el nuevo mensaje

```bash
curl http://froga-176577402.us-east-1.elb.amazonaws.com:8000/health-check/
{"message":"OK","environment":"production","debug":false,"test":false}
curl http://froga-176577402.us-east-1.elb.amazonaws.com:8000/health-check/
{"message":"OKv2","environment":"production","debug":false,"test":false}
curl http://froga-176577402.us-east-1.elb.amazonaws.com:8000/health-check/
{"message":"OKv2","environment":"production","debug":false,"test":false}
```
Ya tenemos la nueva versión desplegada.

## Despliegue en el pipeline 

El siguiente paso es insertar este script en el pipeline creando un stage de Deploy. Este stage deploy lo ideal sería lanzarlo despues de un mergue request por ejemplo. De momento nosotros lo pondremos con 
activación manual;-)

Lo primero es crear para el pipeline las variables de entorno. Debido al funcionamiento de AWS Academy, como las credenciales varían cada vez que comenzais un classroom, tendreis que modificar el valor de sus variables.

![CD](doc/images/cd1.png)
![CD](doc/images/cd2.png)
![CD](doc/images/cd3.png)

Y la varibale IMAGE_TAG en este caso lo defineremos en el propio pipeline, utilizando el sha del commit.

El siguiente paso es crear el stage de Deploy junto con el job. De momento pondremos este job con ejecución manual.
Como imagen base para estos pasos, de momento seleccionaremos una imagen de Gitlab que ya tiene preinstalado el CLI de AWS. 

```yaml
  DEPLOY_TAG: ${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}

deploy:
  stage: deploy
  image: docker:stable
  services:
    - docker:dind
  variables:
    DOCKER_DRIVER: overlay2
  before_script:
    - apk add --update --no-cache jq py-pip
    - pip install awscli


  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull $IMAGE_LATEST
    - docker tag  $IMAGE_LATEST $REPOSITORY_URL:$DEPLOY_TAG
    - echo $AWS_SHARED_CREDENTIALS_FILE
    - aws ec2 describe-instances --region us-east-1
    - $(aws ecr get-login --no-include-email --region us-east-1)
    - docker push  $REPOSITORY_URL:$DEPLOY_TAG
    - echo `aws ecs describe-task-definition --task-definition  $CI_AWS_ECS_TASK_DEFINITION --region us-east-1` > input.json
    - echo $(cat input.json | jq '.taskDefinition.containerDefinitions[].image="'$REPOSITORY_URL':'$DEPLOY_TAG'"') >  input.json
    - echo $(cat input.json | jq '.taskDefinition') > input.json
    - echo $(cat input.json | jq 'del(.taskDefinitionArn) | del(.revision) | del(.status) | del(.requiresAttributes) | del(.compatibilities) | del(.registeredAt)  | del(.registeredBy)')  > input.json
    - cat input.json
    - aws ecs register-task-definition --cli-input-json file://input.json --region us-east-1
    - revision=$(aws ecs describe-task-definition --task-definition $CI_AWS_ECS_TASK_DEFINITION --region us-east-1 | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//' | cut -d "," -f 1)
    - echo $revision
    - aws ecs update-service --cluster $CI_AWS_ECS_CLUSTER --service $CI_AWS_ECS_SERVICE  --task-definition $CI_AWS_ECS_TASK_DEFINITION:$revision --region us-east-1
    - echo "Done Revision ===> ${revision}"
  only:
    refs:
      - master
      #- tags
    changes:
      - project/**/*
      - .gitlab-ci.yml
```


Y ahora ya solo queda probar el nuevo endpoint. Para hacer curl al nuevo endpoint en lugar de hacer curl a la ip 
directamente crearemos un pequeño script que partiendo de la informacion del servicio ECS obtienen la IP del ELB, 
espera a que finalice el rollout y finalmente hace curl al endpoint helth-check. Ya que este script podría ser un pequeño smoke test lo denominamos **smokeTest.sh**

```bash
#!/bin/bash
target_group_arn=`aws ecs describe-services --cluster froga  --services froga  --region us-east-1 | jq .services[].loadBalancers[].targetGroupArn|  tr -d '"'`
elb_arn=`aws elbv2  describe-target-groups --target-group-arn ${target_group_arn} --region us-east-1| jq .TargetGroups[].LoadBalancerArns[] | tr -d '"'`
elb_ip=`aws elbv2 describe-load-balancers --load-balancer-arns ${elb_arn} --region us-east-1 | jq .LoadBalancers[].DNSName | tr -d '"'`
echo $elb_ip
echo "Waiting for ECS task update..."
aws ecs wait services-stable --cluster froga --services froga --region us-east-1
url="http://${elb_ip}:8000/health-check/"
echo $url
curl -X 'GET' \
   $url\
  -H 'accept: application/json'
```

También podemos hacer un smoke test algo más programático. Para ello podemos escribir tests EndToEnd(e2e). Para esto 
utilizaremos la carpeta **tests/e2e**. para que estos test funcionen nos hara falta hacer los request desde fuera 
del servidor fastapi. Y para ello nos hace falta tener la url de despliegue. Además estos tests en el pipeline sólo los 
queremos ejecutar después del despliegue. Por ello cuando en la fase test del pipeline le diremos que no ejecute 
los tess de la carpeta **e2e**.

```yml
- pytest tests -p no:warnings --ignore=tests/e2e
```

Y en deploy agregaremos por un lado pytest  
```yml
- pip install pytest
```
y por otro lado al **smokeTest.sh**. Para poder pasarle el parámetro **--base-url**, hemos creado un fixture
en el conftest.py de e2e que puede recibir un parámetro desde el commandline y después lo establece con fixture;-)
```bash
elb_url="http://${elb_ip}:8000"|tr -d '"'
cd project/tests/e2e
pytest -p no:warnings --base-url=$elb_url
```
Y para que pytest no coja los otros conftest.py, creamos un pytest.ini vacío en la carpeta de **e2e**.

```python
import pytest

def pytest_addoption(parser):
    parser.addoption(
        '--base-url', action='store', default='http://localhost:8000', help='Base URL for the API tests'
    )


@pytest.fixture
def base_url(request):
    return request.config.getoption('--base-url')

```


Ahora que tenemos un pipeline que dura bastante, limitaremos su ejecución a commits con tags. De esta manera solo lanzaremos el pipleine cuando tageemos.

```yml
only:
    refs:
      #- master
      - tags
    changes:
      - project/**/*
      - .gitlab-ci.yml
  except:
    - branches
```

Por otro lado para evitar que al cambiar código no cumplamos con el 
job de test es conveniente llamar al script **localPipeline.sh** que ejecute en local esta tarea. Este script lo habeis creado anteriormente.

```bash
docker-compose exec web pytest tests -p no:warnings --ignore=tests/e2e
docker-compose exec web flake8
docker-compose exec web black . --check
docker-compose exec web /bin/sh -c "isort ./*/*.py --check-only"
```

Si ejecutamos ahora vemos, que el código de test de e2e tiene algún fallo de Linter Flake8;-)
```bash
=============================================== 22 passed in 4.62s ================================================
./tests/e2e/conftest.py:2:1: E265 block comment should start with '# '
./tests/e2e/conftest.py:4:1: E302 expected 2 blank lines, found 1
./tests/e2e/conftest.py:13:1: W391 blank line at end of file
./tests/e2e/test_main.py:1:1: F401 'pytest' imported but unused
./tests/e2e/test_main.py:28:1: W391 blank line at end of file
would reformat tests/e2e/conftest.py
would reformat tests/e2e/test_main.py
```

Reformateamos con black y corregimos los errores de formato de código, ya tenemos el pipeline preparado.
Hacemos push y si queremos ejecutar el pipeline tageamos;-)

```bash
git commit -m 'Pipeline CI/CD v1'
git push origin master
git tag -a v0.11.7 -m 'Pipeline CI/CD v1'
git push origin master --tags 
```


Para evitar instalar el CLI de aws, podemos utilizar una imagen docker que nos proporciona GitLab, y así no tenemos que instalar el CLI. 

```yml
deploy:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  script:
   - aws s3 ...
   - aws  ...
  environment: production
```
Otra posibilidad es crear una imagen nosotros y utilizarla en los diferentes stages de CI;-)

# Mlops  gitlab y dvc

Un buen puento de partida es 

https://gitlab.com/bertbesser/dvc-remote-training

...