FROM python:3.9.0-slim-buster


RUN apt-get update \
  && apt-get -y install netcat gcc \
  && apt-get clean



RUN mkdir /yolov5-fastapi

COPY requirements.txt /yolov5-fastapi

COPY . /yolov5-fastapi

WORKDIR /yolov5-fastapi


# install python dependencies
RUN pip install --upgrade pip

RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]